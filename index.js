var express = require('express');
var app 	= express();
var http 	= require('http');
var https 	= require('https');
var myCfg	= require('./config.json');
var myHost	= myCfg.API_HOST;

var conn	= http;
var myPort	= 80;
if(myCfg.HTTPS){
	conn	= https;
	myPort	= 443;
}

app.get('/:sec', function (req, response) {
	console.log('Hello World! = ' + req.params.sec);
  
	response.send("OK");
});

app.get('/text-process/:textID', function (req, response) {
	var options = {
		host: myHost,
		port: myPort,
		path: "",
		method: 'GET',
		json:true
	};
	
	options.path	= '/api/text-process/hw/' + req.params.textID;
	conn.get(options, function(res) {
		res.on('data', function (chunk) {
			console.log("HW: " + chunk);
		});
	}).on('error', function(e) {
		console.log("Got error: " + e.message);
	});
	
	response.send("OK");
});

app.listen(3000, function () { 
	console.log('Running on port 3000!');
});